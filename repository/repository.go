package repository

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/Vaqif/gin/model"
)

type Repository struct {
	Authorization   IAuthorizationRepository
	Car             ICarRepository
	RentHistory     IRentHistoryRepository
}

func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		Authorization: NewAuthRepository(db),
		Car: NewCarRepository(db),
		RentHistory: NewRentRepository(db),
	}
}

type IAuthorizationRepository interface {
	AddNewUser(user model.SignUpRequest) error
	GetUserByCredentials(username,password string) (model.User,error)
}

type ICarRepository interface {
	AddNewCar(list model.Car) (int64,error)
	GetCars() ([]model.Car,error)
	GetById(carId int) (model.Car,error)
	Delete(carId int) error
	Update(car model.Car) (int64, error)
}

type IRentHistoryRepository interface {
	AddNewHistory(rentHistory model.RentHistory) (int64,error)
	GetHistories(carId int64) ([]model.RentHistory,error)
	DeleteHistory(carId int64) error
	UpdateHistory(car model.RentHistory) error
}


