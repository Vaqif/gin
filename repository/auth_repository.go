package repository

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/Vaqif/gin/model"
)

type AuthRepository struct {
	db *sqlx.DB
}

func (authRepository *AuthRepository) AddNewUser(user model.SignUpRequest) error {
	query := new(QueryBuilder).InsertIntoQuery(userTable,"$1,$2,$3").Build()
	_, err := authRepository.db.Exec(query, user.Username, user.Password, user.Email)
	return err
}

func NewAuthRepository(db *sqlx.DB) *AuthRepository {
	return &AuthRepository{ db: db }
}

func (authRepository *AuthRepository) GetUserByCredentials(username, password string) (model.User, error) {
	var user model.User
	query := new(QueryBuilder).SelectQuery(userTable).WhereQuery("username",username).AndWhereQuery("password",password).Build()
	err := authRepository.db.Get(&user, query)
	return user,err
}
