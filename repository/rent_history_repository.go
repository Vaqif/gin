package repository

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/Vaqif/gin/model"
	"strings"
	"time"
)

type RentHistoryRepository struct {
	db *sqlx.DB
}

func NewRentRepository(db *sqlx.DB) *RentHistoryRepository {
	return &RentHistoryRepository{
		db: db,
	}
}

func (r RentHistoryRepository) AddNewHistory(rentHistory model.RentHistory) (int64,error){
	query := new(QueryBuilder).InsertIntoQuery(rentHistories, "$1,$2,$3").Build()
	res, err := r.db.Exec(
		query,
		strings.Split(time.Time.String(rentHistory.StartDate)," ")[0],
		strings.Split(time.Time.String(rentHistory.EndDate)," ")[0],
		rentHistory.CarId)
	if err != nil {
		return -1, err
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil{
		return 0, err
	}
	return rowsAffected, err
}

func (r RentHistoryRepository) GetHistories(carId int64) ([]model.RentHistory,error){
	var histories []model.RentHistory
	query := new(QueryBuilder).SelectQuery(rentHistories).WhereQuery("car_id",carId).Build()
	err := r.db.Select(&histories, query)
	if err != nil {
		return nil, err
	}
	return histories,nil
}

func (r RentHistoryRepository) DeleteHistory(historyId int64) error{
	query := new(QueryBuilder).DeleteQuery(rentHistories).WhereQuery("id",historyId).Build()
	_, err := r.db.Exec(query)
	return err
}

func (r RentHistoryRepository) UpdateHistory(rentHistory model.RentHistory) error{
	query := new(QueryBuilder).
		UpdateQuery(rentHistories,"start_date", model.ToDateFormat(rentHistory.StartDate)).
		AndUpdateQuery("end_date", model.ToDateFormat(rentHistory.EndDate)).
		WhereQuery("id",rentHistory.Id).
		Build()

	_, err := r.db.Exec(query)
	if err != nil {
		return err
	}
	return nil
}
