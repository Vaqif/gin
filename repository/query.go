package repository

import (
	"fmt"
	"time"
)

type QueryBuilder struct {
	sql string
}

func (builder QueryBuilder) SelectQuery(table string) QueryBuilder {
	builder.sql += fmt.Sprintf("select * from %s",table)
	return builder
}

func (builder QueryBuilder) WhereQuery(paramName, paramValue interface{}) QueryBuilder {
	switch paramValue.(type) {
		case int,int64 : 	 builder.sql += fmt.Sprintf(" where %s = %d", paramName,paramValue)
		case string,time.Time: builder.sql += fmt.Sprintf(" where %s = '%s' ", paramName,paramValue)
	}
	return builder
}

func (builder QueryBuilder) AndWhereQuery(paramName, paramValue interface{}) QueryBuilder {
	switch paramValue.(type) {
		case int,int64 : 	 builder.sql += fmt.Sprintf(" and %s = %d", paramName,paramValue)
		case string,time.Time: builder.sql += fmt.Sprintf(" and %s = '%s' ", paramName,paramValue)
	}
	return builder
}

func (builder QueryBuilder) InsertIntoQuery(tableName string, values string) QueryBuilder {
	builder.sql += fmt.Sprintf("insert into %s values((select nextval('%s_id_seq')),%s) RETURNING id;", tableName, tableName, values)
	return builder
}

func (builder QueryBuilder) DeleteQuery(tableName string) QueryBuilder {
	builder.sql += fmt.Sprintf("delete from %s",tableName)
	return builder
}

func (builder QueryBuilder) UpdateQuery(tableName string, paramName string, paramValue interface{}) QueryBuilder {
	switch paramValue.(type) {
		case int,int64:
			builder.sql += fmt.Sprintf("update %s set %s = %d", tableName, paramName, paramValue)
		case string,time.Time:
			builder.sql += fmt.Sprintf("update %s set %s = '%s'", tableName, paramName, paramValue)
	}
	return builder
}

func (builder QueryBuilder) AndUpdateQuery(paramName string, paramValue interface{}) QueryBuilder {
	switch paramValue.(type) {
	case int,int64:    		builder.sql +=  fmt.Sprintf(", %s = %d", paramName, paramValue)
	case string,time.Time:  builder.sql +=  fmt.Sprintf(", %s = '%s'", paramName, paramValue)
	}
	return builder
}

func (builder QueryBuilder) Build() string {
	return builder.sql
}

