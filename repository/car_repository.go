package repository

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/Vaqif/gin/model"
)

type CarRepository struct {
	db *sqlx.DB
}

func NewCarRepository(db *sqlx.DB) *CarRepository {
	return &CarRepository{
		db: db,
	}
}

func (c CarRepository) AddNewCar(car model.Car) (int64,error){
	query := new(QueryBuilder).InsertIntoQuery(cars,"$1,$2,$3").Build()
	res,err := c.db.Exec(query, car.Company, car.Model, car.DailyCost)
	if err != nil {
		return -1, err
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}
	return rowsAffected,nil
}

func (c CarRepository) GetCars() ([]model.Car,error){
	query := new(QueryBuilder).SelectQuery(cars).Build()
	var car []model.Car
	err := c.db.Select(&car, query)
	if err != nil{
		return nil, err
	}
	return car,err
}
func (c CarRepository) GetById(carId int) (model.Car,error){
	query := new(QueryBuilder).SelectQuery(cars).WhereQuery("id", carId).Build()
	var car model.Car
	row := c.db.QueryRowx(query)
	err := row.Scan(&car.Id,&car.Company,&car.Model,&car.DailyCost)
	if err != nil {
		return model.Car{}, err
	}
	return car,err
}

func (c CarRepository) Delete(carId int) error{
	query := new(QueryBuilder).DeleteQuery(cars).WhereQuery("id",carId).Build()
	_ , err := c.db.Exec(query)
	return err
}
func (c CarRepository) Update(car model.Car) (int64, error) {
	query := new(QueryBuilder).
		UpdateQuery(cars,"company",car.Company).
		AndUpdateQuery("daily_cost",car.DailyCost).
		AndUpdateQuery("model",car.Model).
		WhereQuery("id",car.Id).
		Build()

	result, err := c.db.Exec(query)
	if err != nil {
		return 0,nil
	}
	affected, _ := result.RowsAffected()
	return affected,nil
}
