package repository

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/Vaqif/gin/configs"
)

const(
	userTable = "users"
	cars = "cars"
	rentHistories = "rent_histories"
)

func NewPostgreDb(config configs.Config) (*sqlx.DB,error){
	db, err := sqlx.Open("postgres", fmt.Sprintf(
		"host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
		config.Host, config.Port, config.Username, config.DBName, config.Password, config.SSLMode),
	)

	if err != nil{
		return nil, err
	}

	if err = db.Ping(); err != nil{
		return nil, err
	}
	return db, nil
}

