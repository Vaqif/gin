package model

type SignUpRequest struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
	Email	 string `json:"email"    binding:"required"`
}

type SignInRequest struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type CarRequest struct {
	Company    string `json:"company"`
	Model      string `json:"model"`
	DailyCost  int	  `json:"dailyCost"`
}

type RentHistoryRequest struct {
	Id 			int64	`json:"id"`
	StartDate   string	`json:"startDate"`
	EndDate 	string	`json:"endDate"`
}
