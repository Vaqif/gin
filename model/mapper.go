package model

import "time"

func (c CarRequest) MapToCar() Car {
	return Car{
		0,
		c.Company,
		c.Model,
		c.DailyCost,
	}
}

func (r RentHistoryRequest) MapToRentHistory() (RentHistory,error) {
	parsedStartDate, err1 := parseTimeString(r.StartDate)
	parsedEndDate, err2 := parseTimeString(r.EndDate)
	if err1 != nil{
		return RentHistory{}, err1
	}
	if err2 != nil {
		return RentHistory{}, err2
	}
	return RentHistory{
		r.Id,
		parsedStartDate,
		parsedEndDate,
		0,
	},nil
}

func parseTimeString(timeString string) (time.Time,error) {
	parsedTime, err := time.Parse("02/01/2006", timeString)
	return parsedTime,err
}

func ToDateFormat(t time.Time) string {
	return t.UTC().Format("02.01.2006")
}
