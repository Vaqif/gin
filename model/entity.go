package model

import (
	"time"
)

type User struct {
	Id       int64    `db:"id"`
	Username string
	Password string
	Email    string
}

type Car struct {
	Id 		   int64
	Company    string
	Model      string
	DailyCost  int    `db:"daily_cost"`
}

type RentHistory struct {
	Id			int64
	StartDate   time.Time	`db:"start_date"`
	EndDate 	time.Time	`db:"end_date"`
	CarId 		int64		`db:"car_id"`
}

