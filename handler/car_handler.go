package handler

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/Vaqif/gin/consts"
	"gitlab.com/Vaqif/gin/model"
	"net/http"
	"strconv"
)

func (handler *Handler) addNewCar(context *gin.Context)  {
	var request model.CarRequest

	if err := context.BindJSON(&request); err != nil {
		BadRequest(context,err)
		return
	}

	rowsAffected, err := handler.service.Car.AddNewCar(request)

	if err != nil {
		BadRequest(context, err)
		return
	}

	context.JSON(http.StatusOK, map[string]interface{}{
		"rows affected": rowsAffected,
	})
}

func (handler *Handler) updateCar(context *gin.Context) {
	var request model.CarRequest
	if err := context.BindJSON(&request); err != nil {
		BadRequest(context,err)
		return
	}
	id, err := strconv.ParseInt(context.Param("id"),10,64)
	if err != nil{
		BadRequest(context,err)
		return
	}

	rowsAffected, err := handler.service.Car.UpdateCar(request.MapToCar(),id)
	if err != nil{
		InternServerError(context)
		return
	}
	context.JSON(http.StatusOK, map[string]interface{}{
		"rows affected": rowsAffected,
	})
}

func (handler *Handler) getCars(context *gin.Context)  {
	carList, err := handler.service.Car.GetCars()
	if err != nil {
		InternServerError(context)
		return
	}
	context.JSON(http.StatusOK, carList)
}

func (handler *Handler) getCarById(context *gin.Context) {
	carId, err := strconv.Atoi(context.Param("id"))
	if err != nil {
		BadRequest(context,errors.New(consts.TYPE_CONVERTING_ERROR))
		return
	}
	car, err := handler.service.Car.GetCarById(carId)
	if err != nil{
		BadRequest(context,err)
		return
	}
	context.JSON(http.StatusOK, car)
}
func (handler *Handler) deleteCar(context *gin.Context) {
	id, err := strconv.Atoi(context.Param("id"))
	if err != nil {
		BadRequest(context,errors.New(consts.TYPE_CONVERTING_ERROR))
		return
	}
	err = handler.service.Car.DeleteCar(id)
	if err != nil {
		BadRequest(context,err)
		return
	}
	context.Status(200)
}

func getUserId(context *gin.Context) int {
	id, ok := context.Get("userId")
	if !ok {
		InternServerError(context)
	}
	return id.(int)
}

func InternServerError(context *gin.Context) {
	NewErrorResponse(context, http.StatusInternalServerError, "User id not found")
}

func PrepareInput(context *gin.Context, input interface{}) {
	if err := context.BindJSON(&input); err != nil {
		BadRequest(context,err)
	}
}

func BadRequest(context *gin.Context, err error) {
	if err != nil {
		NewErrorResponse(context, http.StatusBadRequest, err.Error())
	}
}
