package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Vaqif/gin/model"
	"strconv"
)

func (handler *Handler) addNewRentHistory(context *gin.Context)  {
	var request model.RentHistoryRequest
	if err := context.BindJSON(&request); err != nil {
		BadRequest(context,err)
		return
	}
	carId, err := strconv.ParseInt(context.Param("id"), 10, 64)
	rentHistory, err := request.MapToRentHistory()
	if err != nil {
		BadRequest(context,err)
		return
	}
	rowsAffected, err := handler.service.RentHistory.AddNewHistory(rentHistory,carId)
	if err != nil {
		InternServerError(context)
		return
	}
	context.JSON(200,map[string]interface{}{
		"rowsAffected": rowsAffected,
	})
}

func (handler *Handler) getRentHistories(context *gin.Context) {
	param := context.Param("id")
	carId, err := strconv.ParseInt(param, 10, 64)
	if err != nil {
		BadRequest(context,err)
		return
	}
	rentHistories, err := handler.service.RentHistory.GetHistories(carId)
	if rentHistories == nil {
		context.JSON(200,"Rent histories is empty")
		return
	}
	context.JSON(200,rentHistories)
}

func (handler *Handler) updateRentHistory(context *gin.Context)  {
	var request model.RentHistoryRequest
	err := context.BindJSON(&request)
	if err != nil {
		BadRequest(context,err)
		return
	}
	rentHistory, err := request.MapToRentHistory()
	if err != nil {
		BadRequest(context,err)
		return
	}
	err = handler.service.RentHistory.UpdateHistory(rentHistory)
	if err != nil {
		InternServerError(context)
		return
	}
	context.Status(200)
}

func (handler *Handler) deleteRentHistory(context *gin.Context) {
	param := context.Param("historyId")
	id, err := strconv.ParseInt(param, 10, 64)
	if err != nil {
		BadRequest(context,err)
		return
	}
	err = handler.service.RentHistory.DeleteHistory(id)
	if err != nil {
		InternServerError(context)
		return
	}
	context.Status(200)
}
