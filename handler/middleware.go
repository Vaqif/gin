package handler

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

const(
	authorizationHeader = "Authorization"
)

func (handler Handler) identify(context *gin.Context) {
	header := context.GetHeader(authorizationHeader)
	if header == "" {
		NewErrorResponse(context, http.StatusUnauthorized, "token not found")
		return
	}

	splitHeader := strings.Split(header, " ")

	if len(splitHeader) != 2 {
		NewErrorResponse(context, http.StatusUnauthorized, "wrong format for token")
		return
	}

	userId, err := handler.service.Authorization.ParseToken(splitHeader[1])

	if err != nil {
		NewErrorResponse(context,http.StatusUnauthorized,err.Error())
		return
	}

	context.Set("userId",userId)
}
