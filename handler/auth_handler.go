package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Vaqif/gin/model"
	"net/http"
)

func (handler *Handler) signUp(context *gin.Context)  {
	var input model.SignUpRequest

	if err := context.BindJSON(&input); err != nil{
		NewErrorResponse(context,http.StatusBadRequest,err.Error())
		return
	}

	err := handler.service.Authorization.CreateUser(input)
	if err != nil {
		InternServerError(context)
		return
	}

	context.Status(201)
}

func (handler *Handler) signIn(context *gin.Context)  {

	var input model.SignInRequest

	if err := context.BindJSON(&input); err != nil{
		BadRequest(context,err)
		return
	}

	token, err := handler.service.Authorization.GenerateToken(input.Username,input.Password)

	if err != nil {
		InternServerError(context)
		return
	}

	context.JSON(http.StatusOK,map[string]interface{}{
		"token" : token,
	})
}
