package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/Vaqif/gin/service"
	"time"
)

type Handler struct {
	service *service.Service
}

func NewHandler(service *service.Service) *Handler {
	return &Handler{service: service}
}

func (handler *Handler) InitRoutes() *gin.Engine {
	router := gin.New()

	auth := router.Group("/auth")
	{
		auth.POST("/sign-in", handler.signIn)
		auth.POST("/sign-up", handler.signUp)
	}

	api := router.Group("/api")
	{
		list := api.Group("/car", handler.identify)
		{
			list.GET("/",      	handler.getCars)
			list.GET("/:id", 		handler.getCarById)
			list.POST("/",		handler.addNewCar)
			list.PUT("/:id",		handler.updateCar)
			list.DELETE("/:id",	handler.deleteCar)

			items := list.Group("/:id/history")
			{
				items.GET("/", 			   handler.getRentHistories)
				items.POST("/",			   handler.addNewRentHistory)
				items.PUT("/",			   handler.updateRentHistory)
				items.DELETE("/:historyId",  handler.deleteRentHistory)
			}
		}
	}
	return router
}

func NewErrorResponse(context *gin.Context, status int, message string)  {
	logrus.Error(message)
	context.AbortWithStatusJSON(status, ApiError{
		message,
		time.Now().UTC().Format("02.01.2006 15:04"),
	})
}

type ApiError struct{
	ErrorMessage string `json:"errorMessage"`
	Timestamp string `json:"timestamp"`
}
