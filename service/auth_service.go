package service

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/Vaqif/gin/model"
	"gitlab.com/Vaqif/gin/repository"
	"time"
)

type AuthService struct {
	repository repository.IAuthorizationRepository
}

func NewAuthService(repository repository.IAuthorizationRepository) *AuthService {
	return &AuthService{repository: repository}
}

func (authService *AuthService) CreateUser(user model.SignUpRequest) error {
	return authService.repository.AddNewUser(user)
}

func (authService *AuthService) GenerateToken(username, password string) (string, error) {
	user, err := authService.repository.GetUserByCredentials(username, password)

	if err != nil {
		return "", err
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &TokenClaims{
		jwt.StandardClaims{
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: time.Now().Add(12 * time.Hour).Unix(),
		},
		user.Id,
	})

	return token.SignedString([]byte("secret-key"))
}

func (authService AuthService) ParseToken(tokenString string) (int64,error) {
	token, err := jwt.ParseWithClaims(tokenString,&TokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok{
			return nil,errors.New("invalid signing method")
		}
		return []byte("secret-key"),nil
	})

	if err != nil{
		return 0, err
	}

	claims := token.Claims.(*TokenClaims)
	return claims.UserId,nil
}

type TokenClaims struct {
	jwt.StandardClaims
	UserId int64
}
