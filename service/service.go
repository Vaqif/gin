package service

import (
	"gitlab.com/Vaqif/gin/model"
	"gitlab.com/Vaqif/gin/repository"
)

type Service struct {
	Authorization 	IAuthorizationService
	Car      	    ICarService
	RentHistory     IRentHistoryService
}

func NewService(repository *repository.Repository) *Service {
	return &Service{
		Authorization: NewAuthService(repository.Authorization),
		Car: NewCarService(repository.Car),
		RentHistory: NewRentHistoryService(repository.RentHistory),
	}
}

type IAuthorizationService interface {
	CreateUser(user model.SignUpRequest) error
	GenerateToken(username string, password string) (string, error)
	ParseToken(token string) (int64,error)
}

type ICarService interface {
	AddNewCar(list model.CarRequest) (int64,error)
	GetCars() ([]model.Car,error)
	GetCarById(carId int) (model.Car,error)
	DeleteCar(carId int) error
	UpdateCar(car model.Car, id int64) (int64, error)
}

type IRentHistoryService interface {
	AddNewHistory(rentHistory model.RentHistory, carId int64) (int64,error)
	GetHistories(carId int64) ([]model.RentHistory,error)
	GetHistoryById(carId,historyId int) (model.RentHistory,error)
		DeleteHistory(historyId int64) error
	UpdateHistory(car model.RentHistory) error
}

