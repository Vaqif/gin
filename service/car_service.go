package service

import (
	"gitlab.com/Vaqif/gin/model"
	"gitlab.com/Vaqif/gin/repository"
)

type CarService struct {
	repository repository.ICarRepository
}

func NewCarService(repository repository.ICarRepository) *CarService {
	return &CarService{
		repository: repository,
	}
}

func (c CarService) AddNewCar(request model.CarRequest) (int64,error){
	return c.repository.AddNewCar(request.MapToCar())
}

func (c CarService) GetCars() ([]model.Car,error){
	return c.repository.GetCars()
}

func (c CarService) GetCarById(carId int) (model.Car,error){
	return c.repository.GetById(carId)
}

func (c CarService) DeleteCar(carId int) error{
	return c.repository.Delete(carId)
}

func (c CarService) UpdateCar(car model.Car, id int64) (int64, error) {
	car.Id = id
	return c.repository.Update(car)
}
