package service

import (
	"gitlab.com/Vaqif/gin/model"
	"gitlab.com/Vaqif/gin/repository"
)

type RentHistoryService struct {
	repository repository.IRentHistoryRepository
}

func NewRentHistoryService(repository repository.IRentHistoryRepository) *RentHistoryService {
	return &RentHistoryService{
		repository: repository,
	}
}

func (r RentHistoryService) AddNewHistory(rentHistory model.RentHistory, carId int64) (int64,error){
	rentHistory.CarId = carId
	return r.repository.AddNewHistory(rentHistory)
}

func (r RentHistoryService) GetHistories(carId int64) ([]model.RentHistory,error){
	return r.repository.GetHistories(carId)
}

func (r RentHistoryService) GetHistoryById(carId,historyId int) (model.RentHistory,error){
	return model.RentHistory{},nil
}

func (r RentHistoryService) DeleteHistory(historyId int64) error{
	return r.repository.DeleteHistory(historyId)
}

func (r RentHistoryService) UpdateHistory(rentHistory model.RentHistory) error{
	return r.repository.UpdateHistory(rentHistory)
}
