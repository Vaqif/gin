package main

import (
	_ "github.com/lib/pq"
	"github.com/spf13/viper"
	"gitlab.com/Vaqif/gin"
	"gitlab.com/Vaqif/gin/configs"
	"gitlab.com/Vaqif/gin/handler"
	"gitlab.com/Vaqif/gin/repository"
	"gitlab.com/Vaqif/gin/service"
	"log"
)

func main() {
	if err := configs.InitConfig(); err != nil{
		log.Fatal(err.Error())
	}

	db, err := repository.NewPostgreDb(configs.NewConfig())

	if err != nil {
		log.Fatal(err)
	}

	r := repository.NewRepository(db)
	s := service.NewService(r)
	h := handler.NewHandler(s)

	server := new(gin.Server)
	if err := server.Run(viper.GetString("server_port"), h.InitRoutes()); err != nil {
		log.Fatal(err.Error())
	}

}
